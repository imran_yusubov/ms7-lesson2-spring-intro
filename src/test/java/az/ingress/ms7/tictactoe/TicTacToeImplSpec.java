package az.ingress.ms7.tictactoe;

import az.ingress.ms7.tictactoe.domain.Step;
import az.ingress.ms7.tictactoe.repo.StepRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TicTacToeImplSpec {

    @Mock
    private StepRepository stepRepository;

    private TicTacToeImpl ticTacToe;

    @BeforeEach
    public void before() {
        ticTacToe = new TicTacToeImpl(stepRepository);
    }

    @Test
    public void whenXOutsideBoardThenRuntimeException() {
        assertThatThrownBy(() -> ticTacToe.play(5, 2))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("X is outside board");
    }

    @Test
    public void whenOOutsideBoardThenRuntimeException() {
        assertThatThrownBy(() -> ticTacToe.play(2, 4))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Y is outside board");
    }

    @Test
    public void whenOccupiedThenRuntimeException() {
        //Arrange
        Step step = new Step();
        step.setXCord(0);
        step.setYCord(0);
        step.setPlayer('X');
        List steps = List.of(step);
        when(stepRepository.findAll()).thenReturn(steps);

        //Act & Assert
        assertThatThrownBy(() -> ticTacToe.play(1, 1))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Box is occupied");
    }

    @Test
    public void givenFirstTurnWhenNextPlayerThenX() {
        assertThat(ticTacToe.nextPlayer())
                .isEqualTo('X');
    }

    @Test
    public void givenLastPlayerXThenNextPlayerO() {
        //Arrange
        Step step = new Step();
        step.setXCord(0);
        step.setYCord(0);
        step.setPlayer('X');
        List steps = List.of(step);
        when(stepRepository.findAll()).thenReturn(steps);

        //Act & Assert
        assertThat(ticTacToe.nextPlayer())
                .isEqualTo('O');
    }

    @Test
    public void whenPlayThenNoWinner() {
        assertThat(ticTacToe.play(1, 1))
                .isEqualTo("No winner");
    }

    @Test
    public void whenPlayAndWholeHorizontalLineThenWinner() {
        //Arrange
        Step x1 = new Step(1L, 0, 0, 'X');
        Step y1 = new Step(2L, 0, 1, 'O');
        Step x3 = new Step(3L, 1, 0, 'X');
        Step y2 = new Step(4L, 1, 1, 'O');
        when(stepRepository.findAll()).thenReturn(List.of(x1, y1, x3, y2));

        //Act & Assert
        assertThat(ticTacToe.play(3, 1)) //X
                .isEqualTo("X is the winner");
    }

    @Test
    public void whenPlayAndWholeVerticalLineThenWinner() {
        //Arrange
        Step x1 = new Step(1L, 1, 0, 'X');
        Step y1 = new Step(2L, 0, 0, 'O');
        Step x3 = new Step(3L, 2, 0, 'X');
        Step y2 = new Step(4L, 0, 1, 'O');
        Step x4 = new Step(5L, 1, 1, 'X');
        when(stepRepository.findAll()).thenReturn(List.of(x1, y1, x3, y2, x4));

        assertThat(ticTacToe.play(1, 3))
                .isEqualTo("O is the winner");
    }

    @Test
    public void whenPlayAndTopBottomDiagonalLineThenWinner() {
        ticTacToe.play(1, 1);
        ticTacToe.play(1, 2);
        ticTacToe.play(2, 2);
        ticTacToe.play(1, 3);
        assertThat(ticTacToe.play(3, 3))
                .isEqualTo("X is the winner");
    }

    @Test
    public void whenPlayAndBottomTopDiagonalLineThenWinner() {
        ticTacToe.play(1, 3);
        ticTacToe.play(1, 1);
        ticTacToe.play(2, 2);
        ticTacToe.play(1, 2);
        assertThat(ticTacToe.play(3, 1))
                .isEqualTo("X is the winner");
    }

    @Test
    public void whenAllBoxesAreFilledThenDraw() {
        ticTacToe.play(1, 1);
        ticTacToe.play(1, 3);
        ticTacToe.play(1, 2);
        ticTacToe.play(2, 1);
        ticTacToe.play(2, 3);
        ticTacToe.play(2, 2);
        ticTacToe.play(3, 2);
        ticTacToe.play(3, 1);
        assertThat(ticTacToe.play(3, 3))
                .isEqualTo("The result is draw");
    }
}
