package az.ingress.ms7.tictactoe;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;

class TicTacToeTest {

    private  TicTacToe ticTacToe;


    @BeforeEach
    public    void before(){
        ticTacToe=new TicTacToe();
    }

    @Test
    public void whenXOutsideBoardThenRuntimeException(){
        assertThatThrownBy(() -> ticTacToe.play(5,2))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("X is outside board");
    }

    @Test
    public void whenYOutsideBoardThenRuntimeException(){
        assertThatThrownBy(() -> ticTacToe.play(2,5))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("O is outside board");
    }

    @Test
    public void whenOccupiedThenRuntimeException(){
        ticTacToe.play(1,2);//x
        assertThatThrownBy(() -> ticTacToe.play(1,2)) //y
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Box is occupied");
    }

    @Test
    public void givenFirstTurnWhenNextPlayerThenX(){
        assertThat(ticTacToe.nextPlayer()).isEqualTo('X');
    }

    @Test
    public void givenLastTurnWasXWhenNextPlayerThenO(){
        ticTacToe.play(1,1);
        assertThat(ticTacToe.nextPlayer());
    }
    @Test
    public void whenPlayAndWholeHorizontalLineThenWinner() {
        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 1); // X
        ticTacToe.play(2, 2); // O
        String actual = ticTacToe.play(3, 1); // X
        assertEquals("X is the winner", actual);
    }
    @Test
    public void whenPlayAndWholeVerticalLineThenWinner() {
        ticTacToe.play(2, 1); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(3, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        String actual = ticTacToe.play(1, 3); // O
        assertEquals("O is the winner", actual);
    }
    @Test
    public void whenPlayAndTopBottomDiagonalLineThenWinner() {
        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        ticTacToe.play(1, 3); // O
        String actual = ticTacToe.play(3, 3); // O
        assertEquals("X is the winner", actual);
    }
    @Test
    public void whenPlayAndBottomTopDiagonalLineThenWinner() {
        ticTacToe.play(1, 3); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(2, 2); // X
        ticTacToe.play(1, 2); // O
        String actual = ticTacToe.play(3, 1); // O
        assertEquals("X is the winner", actual);
    }
    @Test
    public void whenAllBoxesAreFilledThenDraw() {
        ticTacToe.play(1, 1);
        ticTacToe.play(1, 2);
        ticTacToe.play(1, 3);
        ticTacToe.play(2, 1);
        ticTacToe.play(2, 3);
        ticTacToe.play(2, 2);
        ticTacToe.play(3, 1);
        ticTacToe.play(3, 3);
        String actual = ticTacToe.play(3, 2);
        assertEquals("The result is draw", actual);
    }
}