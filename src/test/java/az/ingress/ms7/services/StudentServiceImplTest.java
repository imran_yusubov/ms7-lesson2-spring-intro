package az.ingress.ms7.services;

import az.ingress.ms7.domain.Student;
import az.ingress.ms7.dto.CreateStudentDto;
import az.ingress.ms7.dto.StudentDto;
import az.ingress.ms7.repository.StudentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StudentServiceImplTest {

    private StudentService studentService;


    @Mock
    private StudentRepository studentRepository;

    @Captor
    private ArgumentCaptor<Student> studentArgumentCaptor;

    @BeforeEach
    void beforeEach() {
        ModelMapper mapper = new ModelMapper();
        studentService = new StudentServiceImpl(studentRepository, mapper);
    }

    @Test
    public void whenNoStudentThenThrowStudentNotFound() {
        //Act & Assert
        assertThatThrownBy(() -> studentService.getStudent(1L))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Student not found");
    }

    @Test
    public void whenGetStudentThenReturnStudentDto() {
        //Arrange
        Student student = Student
                .builder()
                .id(2L)
                .age(20L)
                .name("Hafiz")
                .build();

        when(studentRepository.findById(2L)).thenReturn(Optional.of(student));

        //Act
        StudentDto studentDto = studentService.getStudent(2L);

        //Assert
        assertThat(studentDto.getName()).isEqualTo("Hafiz");
        assertThat(studentDto.getId()).isEqualTo(2L);
        assertThat(studentDto.getAge()).isEqualTo(20);
    }

    @Test
    public void whenGetStudentWithAgeLessThan18ThenThrowException() {
        //Arrange
        Student student = Student
                .builder()
                .id(2L)
                .age(6L)
                .name("Hafiz")
                .build();

        when(studentRepository.findById(2L)).thenReturn(Optional.of(student));

        //Act & Assert
        assertThatThrownBy(() -> studentService.getStudent(2L))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("You don't pass age requirement");
    }


    @Test
    void whenCreateStudentThenNameUpperCase() {
        //Arrange
        CreateStudentDto studentDto = new CreateStudentDto();
        studentDto.setName("hafiz");
        studentDto.setAge(30L);

        Student student = new Student();
        student.setName("HAFIZ");
        student.setAge(30L);
        when(studentRepository.save(student)).thenReturn(student);

        //Act
        studentService.create(studentDto);

        //Assert
        verify(studentRepository, times(2))
                .save(studentArgumentCaptor.capture());
        Student value = studentArgumentCaptor.getValue();
        assertThat(value.getName()).isEqualTo("HAFIZ");
    }


    @Test
    void whenCreateStudentThenNameUpperCaseCaptor() throws ParseException {
        //Arrange
        CreateStudentDto studentDto = new CreateStudentDto();
        studentDto.setName("hafiz");
        studentDto.setAge(30L);

        Student student = new Student();
        student.setName("hafiz");
        student.setAge(30L);
        String sDate6 = "31-Dec-1998 23:37:50";
        SimpleDateFormat formatter6 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        Date parse = formatter6.parse(sDate6);
        student.setCreationDate(parse);
        System.out.println("Student date:"+student.getCreationDate());
        System.out.println("Student before test :" + student);
        when(studentRepository.save(any())).thenReturn(student);

        //Act
        studentService.create(studentDto);

        //Assert
        verify(studentRepository, times(1)).save(studentArgumentCaptor.capture());

        Student student1 = studentArgumentCaptor.getValue();

        assertThat(student1.getCreationDate()).isNotNull();
        System.out.println(student1.getCreationDate());
        System.out.println("Student is after test :" + student1);
        System.out.println("The date is :" + student1.getCreationDate());
    }
}