package az.ingress.ms7.creational_patterns;

public interface Animal {

    void makeNoise();
}
