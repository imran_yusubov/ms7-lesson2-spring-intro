package az.ingress.ms7.creational_patterns;

public class Main {

    public static void main(String[] args) {
        Singleton s = Singleton.getInstance();
        System.out.println(s);
        Singleton s1 = Singleton.getInstance(); //new instance
        System.out.println(s1);


        Animal animal = FactoryMethod.getInstance();


    }
}
