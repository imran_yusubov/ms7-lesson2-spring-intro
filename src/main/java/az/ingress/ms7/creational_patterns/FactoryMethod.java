package az.ingress.ms7.creational_patterns;

public class FactoryMethod {

    public static Animal getInstance() {
        return new Cat();
    }
}
