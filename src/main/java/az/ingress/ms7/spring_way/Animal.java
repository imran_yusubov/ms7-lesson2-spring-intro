package az.ingress.ms7.spring_way;

public interface Animal {

    public void makeNoise();
}
