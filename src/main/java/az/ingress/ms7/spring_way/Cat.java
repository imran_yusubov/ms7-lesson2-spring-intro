package az.ingress.ms7.spring_way;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("cat")
@Scope("prototype")
public class Cat implements Animal{

    @Override
    public void makeNoise() {

    }
}
