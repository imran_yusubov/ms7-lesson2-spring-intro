package az.ingress.ms7.spring_way;

import java.util.Objects;

public class Car {

    //SOLID = >
    //S: Single Responsibility
    //O: Open Close
    //L: Liskov Substitution
    //I: Interface Segregation
    //D: Dependency Inversion
    private Long id;

    private String make;

    public static void main(String[] args) {
        Car car1=new Car();
        car1.id=1L;

        Car car2=new Car();
        car2.id=1L;

        System.out.println(car1.equals(car2)); //

        System.out.println(car1.hashCode());
        System.out.println(car2.hashCode());
    }

}
