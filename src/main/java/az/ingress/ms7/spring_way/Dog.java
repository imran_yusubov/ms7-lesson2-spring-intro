package az.ingress.ms7.spring_way;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component("dog")
public class Dog implements Animal {
    @Override
    public void makeNoise() {

    }
}
