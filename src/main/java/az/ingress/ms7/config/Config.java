package az.ingress.ms7.config;

import az.ingress.ms7.spring_way.Animal;
import az.ingress.ms7.spring_way.Cat;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

@Configuration
public class Config {

    @Bean("mapper")
    @Primary
    public ModelMapper getModelMapper() {
        return new ModelMapper();
    }

//    @Bean
//    @Scope("prototype")
//    public Animal createAnimal() {
//        Cat cat = new Cat();
//        System.out.println("Cat is ->" + cat);
//        return cat;
//    }
}
