package az.ingress.ms7.services;

import az.ingress.ms7.domain.Student;
import az.ingress.ms7.dto.CreateStudentDto;
import az.ingress.ms7.dto.StudentDto;
import az.ingress.ms7.repository.StudentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final ModelMapper mapper;

    public StudentServiceImpl(StudentRepository studentRepository, ModelMapper mapper) {
        this.studentRepository = studentRepository;
        this.mapper = mapper;
    }

    @Override
    public StudentDto getStudent(Long id) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found"));
        if (student.getAge() > 18) {
            return mapper.map(student, StudentDto.class);
        } else {
            throw new RuntimeException("You don't pass age requirement");
        }
    }

    @Override
    public StudentDto create(CreateStudentDto studentDto) {
        studentDto.setName(studentDto.getName().toUpperCase());
        Student student = mapper.map(studentDto, Student.class);
        student.setCreationDate(new Date());
        Student saved = studentRepository.save(student);

        return mapper.map(saved, StudentDto.class);
    }

    @Override
    public StudentDto update(Long id, CreateStudentDto studentDto) {
        studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found"));
        Student student = mapper.map(studentDto, Student.class);
        student.setId(id);
        studentRepository.save(student);
        return mapper.map(student, StudentDto.class);
    }

    @Override
    public void delete(Long id) {
        studentRepository.deleteById(id);
    }
}

// Class A.a() -> B.b() void : was the method called
// Class A.a() -> B.b() Object:
// Class A.a() -> B.b(int a): 1
