package az.ingress.ms7.web.rest;

import az.ingress.ms7.dto.StudentDto;
import az.ingress.ms7.dto.WelcomeMessageDto;
import az.ingress.ms7.services.StudentService;
import az.ingress.ms7.services.StudentServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/hello")
public class HelloController {

    //Di
    private StudentService studentService;

    @GetMapping
    public WelcomeMessageDto sayHello(@RequestHeader("Accept-Language") String lang,
                                      @RequestBody StudentDto studentDto) {
        log.trace("Received request : {}", studentDto);

        List<String> list =List.of("a","b","c");
        list.add("A");
        list.add("B");


        list.stream().forEach(System.out::println);  //


        return lang.equalsIgnoreCase("AZ") ?
                new WelcomeMessageDto("Salam " + studentDto.getName())
                : new WelcomeMessageDto("Hello " + studentDto.getName());
    }

}

