package az.ingress.ms7.tictactoe.repo;

import az.ingress.ms7.tictactoe.domain.Step;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StepRepository extends JpaRepository<Step, Long> {
}
