package az.ingress.ms7;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@RequiredArgsConstructor
@SpringBootApplication
public class Ms7Application {

//    private final ApplicationDetails app;
//
//    private final Animal animal; //null
//
//    private final Animal animal1; //null


    public static void main(String[] args) {
        SpringApplication.run(Ms7Application.class, args);
    }

//    @Override
//    public void run(String... args) throws Exception {
//        System.out.println("Animal is :" + animal);
//        System.out.println("Animal1 is :" + animal1);
//
//        Student student = Student.builder()
//                .age(10L)
//                .name("hdsfv")
//                .build();
//        System.out.println("Application name :" + app.getApplicationName());
//        System.out.println("Application version :" + app.getVersion());
//        System.out.println("Application dev names :" + app.getDeveloperNames());
//    }
}
