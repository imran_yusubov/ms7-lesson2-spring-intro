package az.ingress.ms7.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WelcomeMessageDto {

    private String message;
}
