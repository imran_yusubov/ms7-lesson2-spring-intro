package az.ingress.ms7.dto;

import lombok.Data;

@Data
public class StudentDto {

    private String name;

    private Long id;

    private Long age;

}
