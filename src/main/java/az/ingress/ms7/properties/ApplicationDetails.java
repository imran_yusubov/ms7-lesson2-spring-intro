package az.ingress.ms7.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "ms7")
public class ApplicationDetails {

    private String applicationName;

    private Integer version;

    private List<String> developerNames;
}
